Opis działania aplikacji
================================

JS-y
-----

bootstrap
----------
Główne zmienne systemowe, mobile, phonegap, click/tap event, dbinit


router (przeglądnięte, @todo - przede wszystkim multi param hash)
-----------------------------------------------------------------
Routing, hash change listener
- init (start) : (@todo to bootstrap ?)
- load controller on hash change - hashCheck
- goToPage - hash change on demand



ui (przeglądnięte, małe @todo)
------------------------------
elementy interfejsu, animacje
- ustalenie rozmiarów viewport i slajdów
- event efektu kliknięcia buttona
- zaznaczanie bieżącej strony w buttonach menu stopki - _startUi
- menu podręczne listy - _listMenu
- ładowanie kontentu formatek z efektem przejścia - loadPageContent
- custom alert (todo)
- zastąpienie systemowych selectów (dla mobile i desktop)
