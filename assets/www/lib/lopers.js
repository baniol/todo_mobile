var Lopers = function(dbName){
	var self = this;
	// constructor
	this._dbName = dbName;
	this._tables = [];
	this._db;
	
	// @todo error if table Name exists in LS

	var i = localStorage.getItem(self._dbName);
	if(i === null){
		this._db = localStorage.setItem(self._dbName,JSON.stringify([]));
		this._db = [];
	}else{
		this._db = JSON.parse(i);
	}

	this.setTable = function(tableName,fields){
		// check if tableName exists
		var c = this.getTableData(tableName);
		// table structure
		var tableStr = {table:tableName,fields:fields};
		this._tables.push(tableStr);
		if(c !== undefined)
			return false;
		// add client ID
		fields.push('cid');
		this._db.push({table:tableName,records:[]});
		this.persistTable();
	};

	/**
	 * Returns called table data
	 * @param str table tableName
	 * @return obj table data
	 */
	this.getTableData = function(table){
		var out;
		for(var i in this._db){
			if(this._db[i].table == table){
				out = this._db[i].records;
			}
		}
		if(out === undefined){
			// @todo throw error - table not found ?
		}
		return out;
	};

	/**
	 * Gets table fields (structure)
	 */
	this.getTableFields = function(table){
		var out;
		for(var i in this._tables){
			if(this._tables[i].table == table){
				out = this._tables[i].fields;
			}
		}
		if(out === undefined){
			// @todo throw error - table not found ?
		}
		return out;
	};
	
	this.insertRecord = function(table,arr,callback){
		if(table === undefined){
			// @todo error - no table name
		}
		// @todo walidacja pól (czy są wymagane, potem może typ danych)
		var el = {};
		var fields = this.getTableFields(table);
		for(var i in fields){
			el[fields[i]] = arr[i];
		}
		// pobranie najwyższego i unikalnego cid
		el['cid'] = this.getFreeCid(table);
		var tableData = this.getTableData(table);

		this.pushRecord(el,table);

		if(typeof callback == 'function'){
			callback.call(this);
		}
	};

	this.pushRecord = function(el,table){
		for(var i in this._db){
			if(this._db[i].table == table){
				this._db[i].records.push(el);
			}
		}
		this.persistTable();
	};
	
	/**
	 * Przepisuje obiekt - wpisuje wszystkie nowe wartości
	 */
	this.editRecord = function(table,arr,key,callback){
		arr.push(key);
		var el = this.getItemByCid(table,key);
		var fields = this.getTableFields(table);
		for(var i=0;i<=arr.length;i++){
			el[fields[i]] = arr[i];
		}
		var index = this.getIndexByCid(table,key);
		var td = this.getTableData(table);
		td[index] = el;
		this.persistTable();
		if(typeof callback == 'function'){
			callback.call(this);
		}
	};
	
	/**
	 * Edytuje dane pole obiektu
	 */
	this.editField = function(cid,field,value){
		var index = this.getIndexByCid(cid);
		this.db[index][field] = value;
		this.persistTable();
	};
	
	/**
	 * Updates specific field
	 */
	this.updateField = function(table,whereField,whereValue,targetField,newValue){
		var record = this.getRecords(table,whereField,whereValue);
		for(var i in record){
			var cid = record[i]['cid'];
			var index = self.getIndexByCid(table,cid);
			var td = this.getTableData(table);
			td[index][targetField]= newValue;
		}
		this.persistTable();
	};

	this.deleteRelated = function(table,whereField,whereValue){
		var rec = this.getRecords(table,whereField,whereValue);
		for(var i in rec){
			var cid = rec[i]['cid'];
			var index = self.getIndexByCid(table,cid);
			var td = this.getTableData(table);
			td.splice(index,1);
			this.persistTable();
		}
	};
	
	/**
	 * Returns first free cid for the new record
	 */
	this.getFreeCid = function(table){
		var tableData = this.getTableData(table);
		var arr = [];
		for(var i in tableData){
			arr.push(tableData[i]['cid']);
		}
		if(arr.length == 0)
			return 1;
		else
			return Math.max.apply(Math,arr) + 1;
	};
	
	this.getIndexByCid = function(table,cid){
		var td = this.getTableData(table);
		var out;
		for(var i in td){
			if(td[i]['cid'] == cid)
				out = i;
		}
		return out;
	};
	
	this.persistTable = function(){
		localStorage.removeItem(self._dbName);
		localStorage.setItem(self._dbName,JSON.stringify(self._db));
	};
	
	/**
	 * Zwraca jednostkę wg klucza
	 * na razie nie używana
	 */
	this.getItemByCid = function(table,cid){
		return this.getRecords(table,'cid',cid)[0];
	};
	
	/**
	 * Zwraca indeks kolekcji na podstawie pola cid
	 */
	this.getIdByCid = function(table,cid){
		var out = null;
		var td = this.getTableData(table);
		for(var i in td){
			if(td[i]['cid'] == cid)
				out = i;
		}
		if(out !== null){
			return out;
		}else{
			// @todo error handler
			return 'error';
		}
	};
	
	/**
	 * Wydobywa wszystkie dane danej tabeli
	 * @todo - in case of 1 element return not array (otherwise [0] needed)
	 */
	this.getRecords = function(table,field,value){
		// get called table
		if(table === undefined){
			// @todo throw error - no table name
		}
		var tableData = this.getTableData(table);
		var out = [];
		for(var i in tableData){
			if(field !== undefined && value !== undefined){
				if(tableData[i][field] == value)
					out.push(tableData[i]);
			}else{
				out.push(tableData[i]);
			}
		}
		return out;
	};
	
	/**
	 * wydobywa wszystkie wartości, wg. zadanego klucza
	 */
	this.getValues = function(table,key){
		var out = [];
		var td = this.getTableData(table);
		for(var i in td){
			var obj = {};
			obj.value = td[i][key];
			obj.key = td[i]['cid'];
			out.push(obj);
		}
		return out;
	};
	
	this.deleteRecord = function(table,cid,callback){
		var index = this.getIdByCid(table,cid);
		var td = this.getTableData(table);
		td.splice(index,1);
		this.persistTable();
		if(typeof callback == 'function'){
			callback.call(this);
		}
	};
	
	this.resetTable = function(){
		this.db = [];
		this.persistTable();
	};
	
	this.deleteTable = function(){
		delete this.db;
		this.persistTable();
	}
	
	/**
	 * @todo - unused for the moment
	 */
	this.countRecords = function(table){
		var td = this.getTableData(table);
		var c = 0;
		for(var i in td){
			c++;
		}
		return c;
	};
}