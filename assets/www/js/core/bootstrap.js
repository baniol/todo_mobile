define(['core/router'],function(router){
	var mobile,
		phoneGap,
		triggerEvent,
		_db,
		_pageItem; // page nr for slider

	var _start = function(){
		phoneGap = (document.location.protocol == "file:");
		mobile = _detectMobile();
		triggerEvent = mobile ? 'tap' : 'click';

		// DB init
		_db = new Lopers('fimo_todos');
		_db.setTable('categories',['name']);
		_db.setTable('todos',['category','name','done']);
	};
	
	/**
	 * Duplicate in router
	 */
	var _goToPage = function(str){
		router.goToPage(str);
	};

	/**
	 * Returns true if PhoneGap or mobile browser
	 */
	var _detectMobile = function(){
		if(phoneGap){
			return true;
		}else{
			var re = new RegExp(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i);
			return re.test(navigator.userAgent);
		}
	};

	var _getParams = function(){
		_start();
		return{
			start:_start,
			mobile:mobile,
			phoneGap:phoneGap,
			triggerEvent:triggerEvent,
			db:_db,
			goToPage:_goToPage,
			getUrlParams:router.getUrlParams
		}
	};
	
	// public return
	return _getParams();
});