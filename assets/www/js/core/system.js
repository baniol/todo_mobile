define(['config'],function(conf){
	
	/**
	 * Throws error to output
	 * @todo recognize if console.log exists
	 */
	var _onError = function(msg){
		if(conf.devOutput == 'console'){
			console.log(msg);
		}
		else if(conf.devOutput == 'alert'){
			alert(msg);
		}
		return false;
	};

	return {
		error:_onError
	}

});