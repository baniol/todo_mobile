define(['require','routes','core/system'],function(require,r,sys){
	var baseHash,
		param,
		param2, // @todo change to multiparam (array?)
		pageWidth,
		slider,
		routes,
		defaultRoute;

	var _start = function(){

		// get defined routes from routes.js
		routes = r.routes;
		defaultRoute = r.defaultRoute;

		// init some ui stuff
		// @todo - to bootstrap ?
		require(['core/ui'],function(ui){
			ui.start();
		});

		window.location.hash = window.location.hash || defaultRoute;

		// hash change listener
		window.addEventListener('hashchange',_hashCheck,false);

		// hash check on page (re)load
		_hashCheck();
	};

	/**
	 * Loads controller on hash change
	 */
	var _hashCheck = function(){
		// @todo - is require ui necessary - maybe replace by global
		require(['core/ui'],function(ui){
			ui.startUi();
			_getUrlParams();
			_loadController(_getController());
		});
	};

	/**
	 * Gets controller name based on input hash
	 */
	var _getController = function(){
		var out;
		for (var i = 0; i<=routes.length; i++){
			var r = routes[i];
			if(r !== undefined){
				if (baseHash == r.hash){
					out = r.controller;
				}
			}
		}
		if(out === undefined){
			sys.error('Controller for '+baseHash+' not found!');
		}
		return out;
	};

	/**
	 * Returns hash params
	 * @todo - change to multi param - array ?
	 */
	var _getUrlParams = function(){
		var p = window.location.hash.split('/');
		if(p.length == 1){
			param = null;
			param2 = undefined;
		}
		else if(p.length == 2){
			param = p[1];
		}
		else if(p.length === 3){	
			param = p[2];
			param2 = p[1];
		}
		baseHash = p[0];
		return{
			hash:baseHash,
			param:param,
			param2:param2
		}
	};

	/**
	 * Loads controller based on hash & specific route (routes.js)
	 */
	var _loadController = function(controllerName){
		require(['controllers/' + controllerName], function(controller){
			controller.start();
		});
	}
	
	/**
	 * Change hash on demand
	 */
	var _goToPage = function(str){
		window.location.hash = str;
	};

	return {
		start:_start,
		getUrlParams:_getUrlParams,
		goToPage:_goToPage
	};
});