define(['core/bootstrap'],function(sys){

	var _generateCategories = function(){
		for(var i=1;i<30;i++){
			sys.cats.insertRecord(['kategoria '+i]);
			_generateTodos(i);
		}
	};

	var _generateTodos = function(cat){
		for(var i=1;i<=cat;i++){
			sys.todos.insertRecord([cat,'Todo '+i,0]);
		}
	};

	return {
		generateCategories: _generateCategories
	}
});