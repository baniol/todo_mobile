define(['core/bootstrap','config'],function(sys,conf){

	var _myScroll = [],
		selectOpened = false,
		_scrollPosY,
		_viewportWidth,
		_slideTilt,
		_currentSlide, // current page item for slider 0,1
		// none|fade|slide
		_pageTransition = conf.pageTransition,
		_fadeTimeout = conf.fadeTimeout;
	
	/**
	 * bootstrap init
	 */
	var _start = function(){
		// get viewport width for further UI manipilations
		_viewportWidth = $(document).width();
		$('#slider-container').width(_viewportWidth);
		// all sliders wrapper
		$('#slider-viewport').width(2*_viewportWidth);
		// slider item
		$('.page-item').width(_viewportWidth);

		//get viewport height
		var vh = $(document).height();
		$('#slider-viewport').height(vh-$('#page-header').height()).parent().show('fast',function(){
			$('#slider-viewport').addClass('slider-transition');
		});
		_currentSlide = 0; // start page item (slide)
		_bindEvents();
	};

	var _bindEvents = function(){
		// active state on buttons for mobile
		$(document).on('tap','.button',function(){
			var el = $(this);
			el.addClass('active');
			setTimeout(function(){
				el.removeClass('active');
			},300);
		});
	};

	/**
	 * Restores the default header bar
	 */
	var _startUi = function(){
		var route = sys.getUrlParams();
		$('.menu-button').removeClass('active');
		// set active footer menu button for current page
		$('a[href='+route.hash+'].menu-button').addClass('active');
	};

	/*
	 * Opens context menu for list item
	 * @param el option bullet
	 * @param actions collection of actions for context menu
	 */
	var _listMenu = function(el,actions){
		_scrollPosY = _myScroll[0].getScrollY();
		var elOffset = el.offset();
		var menu = $('<div class="list-context-menu"></div>');
		var del = $('<span class="item delete box-shadow"><i class="icon-trash"></i></span>');
		var edit = $('<span class="item edit box-shadow"><i class="icon-edit"></i></span>');
		menu.append(edit).append(del);
		menu.css({top:elOffset.top,left:elOffset.left});

		// clickable layer (for click outside remove)
		var clickLayer = $('<div class="click-layer" />');
		$('body').append(clickLayer);
		clickLayer.on(sys.triggerEvent,function(){
			_removeListMenu();
		});

		// @todo more custom
		el.removeClass('icon-chevron-left').addClass('icon-chevron-right').addClass('transformed');
		setTimeout(function(){
			$('body').append(menu);
			// edit & delete listeners
			$(document).off(sys.triggerEvent,'.list-context-menu .delete');
			$(document).on(sys.triggerEvent,'.list-context-menu .delete',function(e){
				cid = el.parent().attr('data-cid');
				if(typeof actions.del == 'function'){
					actions.del.call(this);
					el.parent().fadeOut('fast');
					setTimeout(function(){
						_removeListMenu();
						_scrollPage(0);	
					},30);
				}
			});
			$(document).off(sys.triggerEvent,'.list-context-menu .edit');
			$(document).on(sys.triggerEvent,'.list-context-menu .edit',function(e){
				cid = el.parent().attr('data-cid');
				setTimeout(function(){
					_removeListMenu();
					if(typeof actions.edit == 'function'){
						actions.edit.call(this);
					}
				},30);
			});
		},100);
	};

	var _removeListMenu = function(){
		$('.opt').removeClass('transformed').removeClass('icon-chevron-right').addClass('icon-chevron-left');
		$('.list-context-menu').remove();
		$('.click-layer').off('click');
		$('.click-layer').remove();
	};

	/**
	 * Loads content html to container
	 * with transition effect
	 */
	var _loadPageContent = function(content){

		switch(_pageTransition){
			case 'fade':
				$('#slider-viewport .page-item').eq(_currentSlide).fadeOut(_fadeTimeout,function(){
					$('#slider-viewport .page-item')
						.eq(_currentSlide)
						.html(content)
						.fadeIn(_fadeTimeout);
				});
			break;
			case 'slide':
				var next = _currentSlide == 0 ? 1 : 0;
				$('.page-item').removeClass('scrollable');
				if(_currentSlide == 1){
					var move = 0;
					$('#slider-viewport .page-item').eq(0).addClass('scrollable').html(content);
					$('#slider-viewport').css('transform','translateX(-'+move+'px)');
				}
				else if(_currentSlide == 0){
					move = _viewportWidth * -1;
					_slideTilt = move;
					$('#slider-viewport .page-item').eq(1).addClass('scrollable').html(content);
					$('#slider-viewport').css('transform','translateX('+move+'px)');
				}
				_currentSlide = next;
			break;
			case 'none':
				$('#slider-viewport .page-item').eq(_currentSlide).html(content);
			break;
		}
	};

	/**
	 * Custom alert
	 */
	var _alert = function(text){
		alert(text);
	};

	/**
	 * Init iscroll for current page
	 * @param string type (destroy)
	 * @param index iscroll instances array
	 */
	var _scrollPage = function(index,type){
		setTimeout(function(){
			var list = $('.scrollable')[0];
			if(_myScroll[index] !== undefined){
				if(type == 'destroy'){
					_myScroll = [];
					// _myScroll[index].destroy();
					// @todo splice ?
					delete _myScroll[index];
				}else{
					_myScroll[index].refresh();
				}
			}
			if(typeof _myScroll[index] == 'undefined' || type == 'destroy'){
 				_myScroll[index] = new iScroll(list,{
					useTransition: true
				});
			}
		},300);
	};

	/**
	 * Replace system select
	 */
	var _select = function(id){
		// @todo replace rigid id by class - multiple selects on page
		var replaceId = "replace-select";
		var oryg = $('#'+id);
		var checked = oryg.find('option:selected').text();
		var rep = $('<div id="'+replaceId+'" class="replace-select box-shadow"><span class="sel-text">'+checked+'</span><span class="a-icon icon-sort-down"></span></div>');
		rep.insertAfter(oryg);
		oryg.css({position:'absolute',left:'-9999px'});
		// select
		// 1) for mobile
		if(sys.mobile){
			$(document).off('click','#'+replaceId);
			$(document).on('click','#'+replaceId,function(){
				setTimeout(function(){
					_openSelect(id);
				},100);
			});
			// on change
			$(document).off('change','#'+id);
			$(document).on('change','#'+id,function(){
				var sel = $(this).find('option:selected').text();
				$('#'+replaceId+' .sel-text').text(sel);
			});
		}else{
			$(document).off('click','#'+replaceId);
			$(document).on('click','#'+replaceId,function(){
				if(!selectOpened)
					_openSelectDesktop(oryg,rep);
				else
					_removeSelectOptions();
			});
		}
	};

	var _openSelect = function(id){
		var dropdown = document.getElementById(id);
		var event;
		event = document.createEvent('MouseEvents');
		event.initMouseEvent('mousedown', true, true, window);
		dropdown.dispatchEvent(event);
	};

	var _openSelectDesktop = function(oryg,rep){
		selectOpened = true;
		var options = oryg.find('option');
		var html = '';
		options.each(function(){
			var o = $(this);
			html += '<li class="box-shadow" data-value="'+o.val()+'">'+o.text()+'</li>';
		});
		var width = rep.width() + 15;
		var wrapper = $('<ul class="select-wrapper" />')
			.css({width:width+'px'})
			.html(html);
		wrapper.insertAfter(rep);
		// clickable layer (for click outside remove)
		var clickLayer = $('<div class="click-layer" />');
		$('body').append(clickLayer);
		clickLayer.on('click',function(){
			_removeSelectOptions();
		});
		wrapper.find('li').on('click',function(){
			var li = $(this);
			var value = li.attr('data-value');
			var text = li.text();
			var current = oryg.find('option[value='+value+']').attr('selected',true);
			rep.find('.sel-text').text(text);
			_removeSelectOptions();
		});
	};

	var _removeSelectOptions = function(){
		selectOpened = false;
		$('.select-wrapper li').off('click');
		$('.select-wrapper').remove();
		$('.click-layer').off('click');
		$('.click-layer').remove();
	};
	
	// public return
	
	return{
		start:_start,
		scrollPage:_scrollPage,
		startUi:_startUi,
		select:_select,
		listMenu:_listMenu,
		alert:_alert,
		loadPageContent:_loadPageContent
	}
});