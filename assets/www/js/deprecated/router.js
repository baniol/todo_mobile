var _hashCheck = function(noTransition){
		require(['ui'],function(ui){
			ui.startUi();
			_getUrlParams();
			// page transition effects
			if(noTransition){
				switch(pageTransition){
					case "none":
						_loadController(_getController());
					break;
					case "slide":
					
						// slider.css({"transform":"translateX(0px)"});
					break;
					case "fade":
						contentWrapper.fadeOut(fadeTimeout,function(){
							_loadController(_getController());
							contentWrapper.fadeIn(fadeTimeout);
						});
					break;
					default:
						_loadController(_getController());
					break;
				}
			}else{
				// slider.css({"transform":"translateX(-"+pageWidth+"px)"});
				_loadController(_getController());
			}
		});
	};

	var _transitionListener = function(el,action){
		el.addEventListener( 'webkitTransitionEnd', function(){_onTransitionEnd(el,action)}, false );
		el.addEventListener( 'transitionend', function(){_onTransitionEnd(el,action)}, false );
		el.addEventListener( 'OTransitionEnd', function(){_onTransitionEnd(el,action)}, false );
	};

	var _onTransitionEnd = function(el,action){
		el.removeEventListener('webkitTransitionEnd', function(){} ,false);
		el.removeEventListener('transitionend', function(){} ,false);
		el.removeEventListener('OTransitionEnd', function(){} ,false);
		action.call(this);
	};