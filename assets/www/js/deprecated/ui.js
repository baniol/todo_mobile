/**
	 * Sets popup canvas for menu
	 * @param cont str content html
	 */
	var _popupMenu = function(){
		var pop = $('<div id="popup-menu" />');
		var popWrapper = $('<div id="pop-menu-wrapper" />');
		// ustalenie wycentrowania w pionie i wysokości
		$(document.body).append(pop);
		$(document.body).append(popWrapper);
		var ph = pop.height();
		var pw = pop.width();
		var top = (sys.viewport.height - ph) / 2 - 80;
		var left = (sys.viewport.width - pw) / 2;
		pop.css({'top':top+'px','left':left+'px'});
		return pop;
	};

	var _removePopMenu = function(){
		$('#pop-menu-wrapper').remove();
		$('#popup-menu').remove();
	};

	/**
	 * Sets position for menu list
	 * @param obj
	 */
	var _menuList = function(obj){
		var positions = obj.positions;
		var list = $('<ul class="popmenu-list" />');
		var pop = _popupMenu();
		for(var i=0;i<positions.length;i++){
			var li = $('<li class="popmenu-pos" />')
				.text(positions[i].label)
				.attr('data-cid',positions[i].cid)
				.addClass(positions[i].action);
			list.append(li);
		}
		
		pop.html(list);
	};

	var _confirm = function(text,callback){
		var popMenu = $('#popup-menu');
		var confHtml = '<div class="conf-top" >'+text+'</div>';
		confHtml += '<div class="conf-bottom" ><span class="conf-ok">OK</span><span class="conf-cancel">Anuluj</span></div>';
		// @todo dopisać w przypadku nieistnienia popupmenu
		if(popMenu.length > 0){
			popMenu.html(confHtml);
		}else{
			var menuCanvas = _popupMenu();
			menuCanvas.html(confHtml);
		}
		// @todo unregister listeners
		$(document).off(sys.triggerEvent,'.conf-ok');
		$(document).on(sys.triggerEvent,'.conf-ok',function(){
			if(typeof callback == 'function'){
				callback.call(this);
			}
		});
		$(document).off(sys.triggerEvent,'.conf-cancel');
		$(document).on(sys.triggerEvent,'.conf-cancel',function(){
			_removePopMenu();
		});
	};