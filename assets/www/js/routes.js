define(function(){
			  
	return {

		routes:[
			{hash:'#categories', controller:'categoryListController'},
			{hash:'#add-category', controller:'addCategoryController'},
			{hash:'#add-todo', controller:'addTodoController'},
			{hash:'#todos', controller:'todoListController'},
			{hash:'#settings', controller:'settingsController'}
		],

		defaultRoute:'#categories'
	}

});