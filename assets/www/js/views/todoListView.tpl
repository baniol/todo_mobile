<li class="list-pos <%=( done == 1 ? "done" : "") %>" data-cid="<%= cid %>">
	<span class="<%=( done == 1 ? "icon-check" : "icon-check-empty") %> todo-checkbox"></span>
	<span class="name-li"><%= name %></span>
	<span class="opt icon-chevron-left button"></span>
</li>