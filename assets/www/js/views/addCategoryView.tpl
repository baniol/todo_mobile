<div class="centered-content">
 	<form id="add-category-form">
	<div class="control-group">
	  <!-- <label class="control-label" for="category-name">Nazwa kategorii</label> -->
	  <div class="controls">
	    <input type="text" class="big-input box-shadow" id="category-name" value="<%= name %>" placeholder="nazwa kategorii">
	  </div>
	</div>
	<div class="control-group">
	  <div class="controls">
	    <button type="submit" class="big-button square-button button"><i class="<%=( edit == 1 ? "icon-edit" : "icon-plus") %>"></i></button>
	  </div>
	</div>
	</form>
</div>