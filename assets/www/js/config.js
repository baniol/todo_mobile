define(function(){

	return {

		// UI settings
		// none|fade|slide
		pageTransition:'slide',
		fadeTimeout:200,

		// developer settings
		devOutput:'console'
	}

});