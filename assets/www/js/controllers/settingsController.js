define(['core/bootstrap','core/ui','core/dev','tpl!views/settingsView.tpl'],function(sys,ui,dev,tpl){

	var _start = function(){
		// set header
		$('#page-header h1').text('Ustawienia');
		var t = _getList();
		ui.loadPageContent(t);
		_bindEvents();
	};
	
	var _bindEvents = function(){
		$(document).off(sys.triggerEvent,'#generate_categories');
		$(document).on(sys.triggerEvent,'#generate_categories',function(e){
			dev.generateCategories();
			alert('OK');
		});

		$(document).off(sys.triggerEvent,'#clear_memory');
		$(document).on(sys.triggerEvent,'#clear_memory',function(e){
			localStorage.clear();
			alert('ok');
		});
	};

	var _getList = function(){
		var t = $('<ul id="settings-list" class="std-list-1" />');
		var all = {
			generate:{name:"Generuj kategorie",id:'generate_categories'},
			clear:{name:"Wyczyść pamięć",id:'clear_memory'}
		};
		$.each(all,function(k,v){
			var li = tpl({name: v.name,id:v.id});
			t.append(li);
		});
		return t;
	};

	return {
		start:_start
	}
});