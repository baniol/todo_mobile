define(['core/bootstrap','core/ui','tpl!views/addCategoryView.tpl'],function(sys,ui,tpl){

	var route;
	
	var _start = function(){
		// set header
		var headerText = 'Nowa kategoria';
		var catName = "";
		route = sys.getUrlParams();
		var edit = false;
		// @todo - ustalić null albo undefined
		if(route.param !== null && route.param !== undefined){
			headerText = 'Edycja kategorii';
			var el = sys.db.getRecords('categories','cid',route.param)[0];
			catName = el.name;
			edit = true;
		}
		// set header text
		$('#page-header h1').text(headerText);
		// load content
		var t = tpl({name:catName,edit:edit});
		ui.loadPageContent(t);
		_bindEvents();
	};
	
	var _bindEvents = function(){
		$(document).off('submit','#add-category-form');
		$(document).on('submit','#add-category-form',function(e){
			e.preventDefault();
			_addCategory();
		});
	};

	var _addCategory = function(){
		var name = $.trim($('#category-name').val());
		if(name == ''){
			ui.alert('Pole nie może być puste!');
			return false;
		}
		// @todo - ustalić null albo undefined
		if(route.param === null || route.param === undefined){
			sys.db.insertRecord('categories',[name],function(){
				sys.goToPage('#categories');
			});
		}else{
			sys.db.editRecord('categories',[name],route.param,function(){
				sys.goToPage('#categories');
			});
		}
	};
	
	return {
		start:_start
	}
});