define(['core/bootstrap','core/ui','tpl!views/todoListView.tpl'],function(boot,ui,tpl){

	var hash,route;
	
	var _start = function(){
		route = boot.getUrlParams();
		// set header
		var c = boot.db.getRecords('categories','cid',route.param)[0];
		$('#page-header h1').text(c.name);
		var t = _getList();
		ui.loadPageContent(t);
		_bindEvents();
		ui.scrollPage(0,'destroy');
	};

	var _bindEvents = function(){
		var cid;
		$(document).off(boot.triggerEvent,'#unit-list .list-pos .opt');
		$(document).on(boot.triggerEvent,'#unit-list .list-pos .opt',function(e){
			e.stopPropagation();
			cid = $(this).parent().attr('data-cid');
			var actions = {
				del: function(){
					boot.db.deleteRecord('todos',cid);
				},
				edit: function(){
					boot.goToPage('#add-todo/'+cid);
				}
			};
			ui.listMenu($(this),actions);
		});

		// mark donne todo item (left checkbox)
		$(document).off(boot.triggerEvent,'.todo-checkbox');
		$(document).on(boot.triggerEvent,'.todo-checkbox',function(){
			cid = $(this).parent().attr('data-cid');
			var el = $('.list-pos[data-cid='+cid+']');
			// pobranie wartości todo z ls
			var ls = boot.db.getRecords('todos','cid',cid)[0];
			var done = ls.done == 1 ? 0 : 1;
			boot.db.updateField('todos','cid',cid,'done',done);
			if(ls.done == 0){
				el.removeClass('done');
				$(this).removeClass('icon-check').addClass('icon-check-empty');
			}
			else{
				el.addClass('done');
				$(this).removeClass('icon-check-empty').addClass('icon-check');
			}
		});
	};
	
	var _getList = function(){
		// @todo - problem : w emulatorze boot.param = null
		var all = boot.db.getRecords('todos',route.param);
		var t = $('<ul id="unit-list" class="std-list-1" />');
		// @todo przerobić na for
		$.each(all,function(k,v){
			var li = tpl({name: v.name,cid:v.cid,done:v.done});
			t.append(li);
		});
		return t;
	};
	
	return {
		start:_start
	}
});