define(['core/bootstrap','core/system','core/ui','tpl!views/addTodoView.tpl'],function(boot,sys,ui,tpl){

	var route;
	
	var _start = function(){
		// @todo - i18n module
		var headerText = 'Nowe todo';
		var todoName = "";
		route = boot.getUrlParams();
		var edit = false;
		// @todo - ustalić null albo undefined & multi param
		if(route.param !== null && route.param !== undefined){
			headerText = 'Edycja todo';
			var el = boot.db.getRecords('todos','cid',route.param)[0];
			todoName = el.name;
			edit = true;
		}
		$('#page-header h1').text(headerText);
		var t = tpl({name:todoName,edit:edit});
		ui.loadPageContent(t);
		_populateSelect();
		_bindEvents();
	};

	var _bindEvents = function(){
		$(document).off(boot.triggerEvent,'.insert-record');
		$(document).on(boot.triggerEvent, '.insert-record',function(){
			_addTodo();
		});
	};
	
	var _populateSelect= function(){
		var selectPac = boot.db.getValues('categories','name');
		// picking current category for select
		var cat = null;
		// @todo - ustalić null albo undefined
		if(route.param !== null && route.param !== undefined){
			// @todo - niech zwraca jeden element (nie będzie trzeba [0])
			var el = boot.db.getRecords('todos','cid',route.param)[0];
			cat = parseInt(el.category);
		}
		setTimeout(function(){
			$.each(selectPac,function(k,v){
				var selected = v.key === cat ? 'selected' : null;
				var s = '<option value="'+v.key+'" '+selected+'>'+v.value+'</option>';
				$('#select-cat').append(s);
			});
			// zamiana selecta kategorii
			ui.select('select-cat');
		},100);
	};
	
	var _addTodo = function(){
		var todoName = $.trim($('#todo-text').val());
		var cat = $('#select-cat option:selected').val();
		if(todoName == ""){
			ui.alert('Wypełnij pole!');
			return false;
		}
		// @todo - ustalić null albo undefined
		if(route.param === null || route.param === undefined){
			boot.db.insertRecord('todos',[cat,todoName,0]);
			_clearText();
		}else{
			// edycja
			boot.db.editRecord('todos',[cat,todoName,0],route.param);
			boot.goToPage('#todos/'+cat,1);
		}
	};
	
	var _clearText = function(){
		$('#todo-text').val("");
	};
	
	return {
		start:_start
	}
});