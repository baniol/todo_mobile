define(['core/bootstrap','core/ui','tpl!views/categoryListView.tpl'],function(boot,ui,tpl){

	var contextMenu;
	
	var _start = function(){
		// set header
		$('#page-header h1').text('Kategorie');
		var t = getList();
		ui.loadPageContent(t);
		_bindEvents();
		ui.scrollPage(0,'destroy');
	};

	var _bindEvents = function(){
		var cid;
		$(document).off(boot.triggerEvent,'#category-list .list-pos .opt');
		$(document).on(boot.triggerEvent,'#category-list .list-pos .opt',function(e){
			e.stopPropagation();
			cid = $(this).parent().attr('data-cid');
			var actions = {
				del: function(){
					boot.db.deleteRecord('categories',cid,function(){
						boot.db.deleteRelated('todos','category',cid);
					});
				},
				edit: function(){
					boot.goToPage('#add-category/'+cid);
				}
			};
			ui.listMenu($(this),actions);
		});
		
		// delete category
		$(document).off(boot.triggerEvent,'._deleteCategory');
		$(document).on(boot.triggerEvent,'._deleteCategory',function(){
			ui.confirm('Na pewno usunąć?',function(){
				ui.removePopMenu();
				var el = $('.list-pos[data-cid='+cid+']');
				boot.cats.deleteUnit(cid,function(){
					boot.todos.deleteConditionnal('category',cid);
					el.fadeOut('fast');
				});
			});
		});

		// edit category
		$(document).off(boot.triggerEvent,'._editCategory');
		$(document).on(boot.triggerEvent,'._editCategory',function(){
			ui.removePopMenu();
			var cid = $(this).attr('data-cid');
			boot.goToPage('#add-category/'+cid);
		});
	};
	
	var getList = function(){
		var all = boot.db.getRecords('categories');
		// @todo get ul wrapper from ui module ?
		var t = $('<ul id="category-list" class="std-list-1" />');
		$.each(all,function(k,v){
			// liczba jednostek dla danej paczki
			/**
			 * @todo może inaczej liczenie - wydajniej (zmienić w empers)
			 */
			var allUnits = boot.db.getRecords('todos','category',v.cid);
			var unitCount = allUnits.length;
			var li = tpl({name: v.name,cid:v.cid,count:unitCount});
			t.append(li);
		});
		return t;
	};
	
	return {
		start:_start
	}
});
